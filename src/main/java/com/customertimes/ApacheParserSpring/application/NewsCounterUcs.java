package com.customertimes.ApacheParserSpring.application;


import com.customertimes.ApacheParserSpring.domain.ResultCount;
import com.customertimes.ApacheParserSpring.domain.Webdata;
import com.customertimes.ApacheParserSpring.services.counterService.NewsCounterService;
import com.customertimes.ApacheParserSpring.services.downloadService.WebPageDownloadService;
import com.customertimes.ApacheParserSpring.services.printService.NewsPrintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NewsCounterUcs {

    @Autowired
    private final NewsCounterService newsCounterService;
    @Autowired
    private final WebPageDownloadService webPageDownloadService;
    @Autowired
    private final NewsPrintService newsPrintService;

    public NewsCounterUcs(NewsCounterService newsCounterService, WebPageDownloadService webPageDownloadService, NewsPrintService newsPrintService) {
        this.newsCounterService = newsCounterService;
        this.webPageDownloadService = webPageDownloadService;
        this.newsPrintService = newsPrintService;
    }

    public void run() {
        final Webdata pageContent = webPageDownloadService.download();
        final ResultCount todayNewsCount = newsCounterService.count(pageContent);
        newsPrintService.printNewsCount(todayNewsCount);
    }
}


