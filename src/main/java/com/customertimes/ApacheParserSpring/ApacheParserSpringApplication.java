package com.customertimes.ApacheParserSpring;

import com.customertimes.ApacheParserSpring.application.NewsCounterUcs;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApacheParserSpringApplication {

	public static void main(String[] args) {
		final ApplicationContext applicationContext = SpringApplication.run(ApacheParserSpringApplication.class, args);
		final NewsCounterUcs counterUcs = applicationContext.getBean(NewsCounterUcs.class);
		counterUcs.run();
	}
}
