package com.customertimes.ApacheParserSpring.services.downloadService;

import com.customertimes.ApacheParserSpring.domain.URL;
import com.customertimes.ApacheParserSpring.domain.Webdata;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("mirknig")
@Component
public class MirknigDownloadServiceImpl extends AbstractDownloadService {

    @Value("${url}")
    private String siteUrl;

    @Override
    public Webdata download() {
        return getSiteContentData(new URL(siteUrl));
    }
}
