package com.customertimes.ApacheParserSpring.services.downloadService;

import com.customertimes.ApacheParserSpring.domain.Webdata;

public interface WebPageDownloadService {

    Webdata download();
}
