package com.customertimes.ApacheParserSpring.services.printService;

import com.customertimes.ApacheParserSpring.domain.ResultCount;
import com.customertimes.ApacheParserSpring.domain.URL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NewsPrintService {

    @Value("${url}")
    private URL url;

    public void printNewsCount(final ResultCount count) {
        System.out.println("The amount of today's news on <" + url.getValidUrl() + "> is <" + count.getCountResult() + ">");
    }
}

