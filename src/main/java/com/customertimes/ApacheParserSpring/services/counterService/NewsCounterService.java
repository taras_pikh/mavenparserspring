package com.customertimes.ApacheParserSpring.services.counterService;

import com.customertimes.ApacheParserSpring.domain.ResultCount;
import com.customertimes.ApacheParserSpring.domain.Webdata;

public interface NewsCounterService {

    ResultCount count(final Webdata content);
}
