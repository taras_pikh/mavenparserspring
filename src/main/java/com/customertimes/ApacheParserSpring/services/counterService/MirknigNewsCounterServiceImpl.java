package com.customertimes.ApacheParserSpring.services.counterService;

import com.customertimes.ApacheParserSpring.domain.ResultCount;
import com.customertimes.ApacheParserSpring.domain.Webdata;
import com.customertimes.ApacheParserSpring.services.utils.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Profile("mirknig")
@Component
public class MirknigNewsCounterServiceImpl implements NewsCounterService {

    @Value("${date_pattern_contains}")
    private String DATE_PATTERN_CONTAINS;

    @Override
    public ResultCount count(final Webdata siteContent) {
        final List<String> contentList = Arrays.asList(siteContent.getWebData().split("\\n"));
        final int contentSize = contentList.size() - 1;
        return new ResultCount(countTodayNews(contentList, contentList.get(contentSize), contentSize, 0));
    }

    int countTodayNews(final List<String> contentList, final String content, final int size, final int counter) {
        if (size == 0) {
            return counter;
        } else if(isTodayDay(content)) {
            return countTodayNews(contentList, contentList.get(size - 1), size - 1, counter + 1);
        }
        return countTodayNews(contentList, contentList.get(size- 1), size - 1, counter);
    }

    private boolean isTodayDay(final String content) {
        return (content.contains("Сегодня, ") && Utils.ifContainsDate(content, DATE_PATTERN_CONTAINS));
    }
}
