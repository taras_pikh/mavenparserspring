package com.customertimes.ApacheParserSpring.domain;

import com.customertimes.ApacheParserSpring.services.utils.Utils;

public class Webdata {

    private final String webHtml;

    public Webdata(final String webHtml) {
        this.webHtml = checkWebHtml(webHtml);
    }

    public String getWebData() {
        return checkWebHtml(this.webHtml);
    }

    private boolean isWebPage(final String webData) {
        return Utils.checkWebPage(webData);
    }

    private String checkWebHtml(final String htmlStr) {
        if (!isWebPage(htmlStr))
            throw new IllegalArgumentException();
        return htmlStr;
    }
}
