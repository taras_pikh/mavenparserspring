package com.customertimes.ApacheParserSpring.domain;

public class ResultCount {

    private final int result;

    public ResultCount(int result) {
        this.result = checkIfValidNumber(result);
    }

    public int getCountResult() {
        return checkIfValidNumber(this.result);
    }

    private int checkIfValidNumber(final int num) {
        if (num < 0)
            throw new IllegalArgumentException();
        return num;
    }
}
