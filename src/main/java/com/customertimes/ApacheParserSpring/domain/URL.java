package com.customertimes.ApacheParserSpring.domain;

public class URL {

    private final String url;

    public URL(final String url) {
        this.url = checkUrl(url);
    }

    public String getValidUrl() {
        return checkUrl(this.url);
    }

    private String checkUrl(final String url) {
        if (!(url == null || url == "" || url.startsWith("https://")))
            throw new IllegalArgumentException();
        return url;
    }
}
